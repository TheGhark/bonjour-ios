//
//  RGSBonjourServicesManager.h
//  Remark
//
//  Created by Camilo Rodriguez Gaviria on 14/03/14.
//  Copyright (c) 2014 Railwaymen. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const RGSBonjourServicesManagerBrowserServiceType;
extern NSString *const RGSBonjourServicesManagerNetServiceTXTRecordDataServiceIdKey;
extern NSString *const RGSBonjourServicesManagerNetServiceTXTRecordDataNaNValue;
extern NSString *const RGSBonjourServicesManagerNetServiceTXTRecordDataMessageDataTooLong;
extern NSString *const RGSBonjourServicesManagerNetServiceTXTRecordDataMessageWillStop;
extern NSString *const RGSBonjourServicesManagerNetServiceTXTRecordDataMessageKey;
extern NSInteger const RGSBonjourServicesManagerNetServicePort;

@class RGSBonjourServicesManager;
@protocol RGSBonjourServicesDelegate <NSObject>

@required
- (void)bonjourServicesManager:(RGSBonjourServicesManager *)bonjourServicesManager didGetServiceId:(NSString *)serviceId;
- (void)bonjourServicesManager:(RGSBonjourServicesManager *)bonjourServicesManager didGetUpdate:(NSString *)message forServiceId:(NSString *)serviceId;
- (void)bonjourServicesManager:(RGSBonjourServicesManager *)bonjourServicesManager willStopPublishingForServiceId:(NSString *)serviceId;
- (void)bonjourServicesManager:(RGSBonjourServicesManager *)bonjourServicesManager didServiceStopPublishing:(NSNetService *)service;

@end

@interface RGSBonjourServicesManager : NSObject

@property (nonatomic, strong, readonly) NSString *serviceId;
@property (nonatomic, strong, readonly) NSArray *services;
@property (nonatomic, weak) id<RGSBonjourServicesDelegate> delegate;

- (instancetype)initWithDelegate:(id<RGSBonjourServicesDelegate>)delegate;
- (void)resolveIPAddress:(NSNetService *)service;
- (NSNetService *)service;
- (void)stop;

@end
