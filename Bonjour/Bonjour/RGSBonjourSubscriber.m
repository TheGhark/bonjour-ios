//
//  RGSBonjourSubscriber.m
//  Remark
//
//  Created by Camilo Rodriguez Gaviria on 17/03/14.
//  Copyright (c) 2014 Railwaymen. All rights reserved.
//

#import "RGSBonjourSubscriber.h"

@interface RGSBonjourSubscriber () <NSNetServiceBrowserDelegate>

@property (nonatomic, weak) RGSBonjourServicesManager *bonjourManager;
@property (nonatomic, strong) NSNetServiceBrowser *serviceBrowser;
@property (nonatomic, strong) NSMutableArray *services;

@end

@implementation RGSBonjourSubscriber

#pragma mark - Life cycle

- (instancetype)initWithBonjourManager:(RGSBonjourServicesManager *)bonjourManager delegate:(id<RGSBonjourSubscriberDelegate>)delegate {
	self = [super init];

	if (self) {
		self.bonjourManager = bonjourManager;
		self.services = [[NSMutableArray alloc] initWithCapacity:0];
		self.serviceBrowser = [[NSNetServiceBrowser alloc] init];
		self.serviceBrowser.delegate = self;
        self.delegate = delegate;
	}

	return self;
}

#pragma mark - NSNetServiceBrowserDelegate

- (void)netServiceBrowser:(NSNetServiceBrowser *)aNetServiceBrowser didFindService:(NSNetService *)aNetService moreComing:(BOOL)moreComing {
    BOOL shouldAdd = YES;
    
    if ([self.delegate respondsToSelector:@selector(bonjourSubscriber:shouldAddService:)]) {
        shouldAdd = [self.delegate bonjourSubscriber:self shouldAddService:aNetService];
    }
    
	if (![self.services containsObject:aNetService] && shouldAdd) {
		[self.services addObject:aNetService];
		[aNetService startMonitoring];

		[self.bonjourManager resolveIPAddress:aNetService];
	}
}

- (void)netServiceBrowser:(NSNetServiceBrowser *)aNetServiceBrowser didRemoveService:(NSNetService *)aNetService moreComing:(BOOL)moreComing {
	if ([self.services containsObject:aNetService]) {
		[self.services removeObject:aNetService];
        
        if ([self.delegate respondsToSelector:@selector(bonjourSubscriber:didRemoveService:)]) {
            [self.delegate bonjourSubscriber:self didRemoveService:aNetService];
        }
	}
}

- (void)netServiceBrowserDidStopSearch:(NSNetServiceBrowser *)aNetServiceBrowser {
	NSLog(@"Service browser stop searching for services.");
	self.serviceBrowser.delegate = nil;
	self.serviceBrowser = nil;
}

#pragma mark - Public

- (void)start {
	[self.serviceBrowser searchForServicesOfType:RGSBonjourServicesManagerBrowserServiceType inDomain:@""];
}

- (void)stop {
	[self.serviceBrowser stop];
}

#pragma mark - Dealloc

- (void)dealloc {
	[self stop];
}

@end
