//
//  RGSBonjourPublisher.m
//  Remark
//
//  Created by Camilo Rodriguez Gaviria on 17/03/14.
//  Copyright (c) 2014 Railwaymen. All rights reserved.
//

#import "RGSBonjourPublisher.h"

@interface RGSBonjourPublisher ()

@property (nonatomic, strong) NSNetService *service;
@property (nonatomic, strong) NSTimer *publishingTimer;
@property (nonatomic, weak) RGSBonjourServicesManager *bonjourManager;
@property (atomic, assign) BOOL shouldStop;

- (void)startPublishingTimer;
- (void)updateTXTForService;
- (NSDictionary *)informationDictionaryForTXTRecord;
- (NSDictionary *)informationDictionaryForTXTRecordWithMessage:(NSString *)message;

@end

@implementation RGSBonjourPublisher

#pragma mark - Life cycle

- (id)initWithBonjourManager:(RGSBonjourServicesManager *)bonjourManager delegate:(id<RGSBonjourPublisherDelegate>)delegate {
    self = [super init];
    
    if (self) {
        self.delegate = delegate;
        self.bonjourManager = bonjourManager;
        self.service = [[NSNetService alloc] initWithDomain:@"" type:RGSBonjourServicesManagerBrowserServiceType name:@"" port:RGSBonjourServicesManagerNetServicePort];
        self.service.delegate = self;
        self.service.TXTRecordData = [NSNetService dataFromTXTRecordDictionary:[self informationDictionaryForTXTRecord]];
    }
    
    return self;
}

#pragma mark - Setter

- (void)setPublishingTimer:(NSTimer *)publishingTimer {
    if (_publishingTimer) {
        [_publishingTimer invalidate];
    }
    
    _publishingTimer = publishingTimer;
}

#pragma mark - NSNetServiceDelegate

- (void)netServiceDidResolveAddress:(NSNetService *)service {
#ifdef DEV_OPS
    NSData *address = nil;
    struct sockaddr_in *socketAddress = nil;
    NSString *ipString = nil;
    int port;
    
    for (int i = 0; i < [[service addresses] count]; i++) {
        address = [[service addresses] objectAtIndex:i];
        socketAddress = (struct sockaddr_in *)[address bytes];
        ipString = [NSString stringWithFormat:@"%s", inet_ntoa(socketAddress->sin_addr)];
        port = socketAddress->sin_port;
        NSLog(@"Resolved: %@-->%@:%d\n", [service hostName], ipString, port);
    }
#endif
}

- (void)netService:(NSNetService *)sender didNotResolve:(NSDictionary *)errorDict {
    NSLog(@"Could not resolve: %@", errorDict);
}

- (void)netService:(NSNetService *)sender didUpdateTXTRecordData:(NSData *)data {
    NSDictionary *info = [NSNetService dictionaryFromTXTRecordData:data];
    NSString *serviceId = [[NSString alloc] initWithData:[info objectForKey:RGSBonjourServicesManagerNetServiceTXTRecordDataServiceIdKey] encoding:NSUTF8StringEncoding];
    NSString *message = [[NSString alloc] initWithData:[info objectForKey:RGSBonjourServicesManagerNetServiceTXTRecordDataMessageKey] encoding:NSUTF8StringEncoding];
    message = (message.length == 0) ? nil : message;
    
    if ([message isEqualToString:RGSBonjourServicesManagerNetServiceTXTRecordDataMessageDataTooLong]) {
        NSLog(@"Unable to track the position of a viewer (%@)\nError: %@", serviceId, message);
    } else if ([message isEqualToString:RGSBonjourServicesManagerNetServiceTXTRecordDataMessageWillStop]) {
        [self.delegate bonjourPublisher:self willStopPublishingForServiceId:serviceId];
    } else {
        [self.delegate bonjourPublisher:self didGetUpdate:message forServiceId:serviceId];
    }
}

- (void)netService:(NSNetService *)sender didNotPublish:(NSDictionary *)errorDict {
    NSLog(@"Could not publish :%@", errorDict);
}

- (void)netServiceDidStop:(NSNetService *)sender {
    NSLog(@"Service stopped publishing.");
    self.service.delegate = nil;
    self.service = nil;
}

#pragma mark - Private

- (void)startPublishingTimer {
    self.publishingTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateTXTForService) userInfo:nil repeats:YES];
}

- (void)updateTXTForService {
    NSDictionary *info = (self.shouldStop) ? [self informationDictionaryForTXTRecordWithMessage:RGSBonjourServicesManagerNetServiceTXTRecordDataMessageWillStop] : [self informationDictionaryForTXTRecord];
    
    for (NSString *key in [info allKeys]) {
        id object = [info objectForKey:key];
        NSData *keyData = [key dataUsingEncoding:NSUTF8StringEncoding];
        NSData *objectData = ([object isKindOfClass:[NSNull class]]) ? nil: [object dataUsingEncoding:NSUTF8StringEncoding];
        
        if ((objectData) && (keyData.length + objectData.length > 255)) {
            info = [self informationDictionaryForTXTRecordWithMessage:RGSBonjourServicesManagerNetServiceTXTRecordDataMessageDataTooLong];
            break;
        }
    }
    
    NSData *data = [NSNetService dataFromTXTRecordDictionary:info];
    
    if (data.length > 65535) {
        info = [self informationDictionaryForTXTRecordWithMessage:RGSBonjourServicesManagerNetServiceTXTRecordDataMessageDataTooLong];
    }
    
    self.service.TXTRecordData = [NSNetService dataFromTXTRecordDictionary:info];
}

- (NSDictionary *)informationDictionaryForTXTRecord {
    NSString *stringFromDate = [NSDateFormatter localizedStringFromDate:[NSDate date] dateStyle:NSDateFormatterFullStyle timeStyle:NSDateFormatterFullStyle];
    return [self informationDictionaryForTXTRecordWithMessage:stringFromDate];
}

- (NSDictionary *)informationDictionaryForTXTRecordWithMessage:(NSString *)message {
    return @{RGSBonjourServicesManagerNetServiceTXTRecordDataServiceIdKey: self.bonjourManager.serviceId,
             RGSBonjourServicesManagerNetServiceTXTRecordDataMessageKey: message};
}

#pragma mark - Public

- (void)start {
    [self.service publish];
    [self.service startMonitoring];
    [self startPublishingTimer];
}

- (void)stop {
    self.shouldStop = YES;
    
    [self updateTXTForService];
    [self.service stop];
    [self.service stopMonitoring];
    [self.publishingTimer invalidate];
}

#pragma mark - Dealloc

- (void)dealloc {
    [self stop];
}

@end
