//
//  RGSBonjourServicesManager.m
//  Remark
//
//  Created by Camilo Rodriguez Gaviria on 14/03/14.
//  Copyright (c) 2014 Railwaymen. All rights reserved.
//

#import "RGSBonjourServicesManager.h"
#import <netinet/in.h>
#import <arpa/inet.h>
#import "RGSBonjourPublisher.h"
#import "RGSBonjourSubscriber.h"

NSString *const RGSBonjourServicesManagerBrowserServiceType = @"_RemarkHQHeads._tcp";
NSString *const RGSBonjourServicesManagerNetServiceTXTRecordDataServiceIdKey = @"serviceId";
NSString *const RGSBonjourServicesManagerNetServiceTXTRecordDataMessageKey = @"message";
NSString *const RGSBonjourServicesManagerNetServiceTXTRecordDataNaNValue = @"NaN";
NSString *const RGSBonjourServicesManagerNetServiceTXTRecordDataMessageDataTooLong = @"Data too long";
NSString *const RGSBonjourServicesManagerNetServiceTXTRecordDataMessageWillStop = @"Will stop";
NSInteger const RGSBonjourServicesManagerNetServicePort = 9876;

@interface RGSBonjourServicesManager () <RGSBonjourSubscriberDelegate, RGSBonjourPublisherDelegate>

@property (nonatomic, strong) RGSBonjourPublisher *publisher;
@property (nonatomic, strong) RGSBonjourSubscriber *subscriber;
@property (nonatomic, strong) NSString *serviceId;
@property (nonatomic, strong) NSArray * services;

@end

@implementation RGSBonjourServicesManager

#pragma mark - Life cycle

- (instancetype)initWithDelegate:(id<RGSBonjourServicesDelegate>)delegate {
    self = [self init];
    
    if (self) {
        self.delegate = delegate;
        self.serviceId = [[NSUUID UUID] UUIDString];
        
        self.publisher = [[RGSBonjourPublisher alloc] initWithBonjourManager:self delegate:self];
        [self.publisher start];
        
        self.subscriber = [[RGSBonjourSubscriber alloc] initWithBonjourManager:self delegate:self];
        [self.subscriber start];
    }
    
    return self;
}

#pragma mark - Getter

- (NSArray *)services {
    _services = self.subscriber.services;
    
    return _services;
}

#pragma mark - RGSBonjourSubscriberDelegate

- (BOOL)bonjourSubscriber:(RGSBonjourSubscriber *)bonjourSubscriber shouldAddService:(NSNetService *)netService {
    NSLog(@"Services:\nLocal: %@\nFound: %@", self.service, netService);
    BOOL shouldAdd = NO;
    
    if (![self.service.name isEqualToString:netService.name]) {
        shouldAdd = YES;
    }
    
    return shouldAdd;
}

- (void)bonjourSubscriber:(RGSBonjourSubscriber *)bonjourSubscriber didRemoveService:(NSNetService *)netService {
    [self.delegate bonjourServicesManager:self didServiceStopPublishing:netService];
}

#pragma mark - RGSBonjourPublisherDelegate

- (void)bonjourPublisher:(RGSBonjourPublisher *)bonjourPublisher didGetServiceId:(NSString *)serviceId {
    [self.delegate bonjourServicesManager:self didGetServiceId:self.serviceId];
}

- (void)bonjourPublisher:(RGSBonjourPublisher *)bonjourPublisher didGetUpdate:(NSString *)message forServiceId:(NSString *)serviceId {
    [self.delegate bonjourServicesManager:self didGetUpdate:message forServiceId:serviceId];
}

- (void)bonjourPublisher:(RGSBonjourPublisher *)bonjourPublisher willStopPublishingForServiceId:(NSString *)serviceId {
    [self.delegate bonjourServicesManager:self willStopPublishingForServiceId:serviceId];
}

#pragma mark - Public

- (void)resolveIPAddress:(NSNetService *)service {
    NSNetService *remoteService = service;
    remoteService.delegate = self.publisher;
    
    [remoteService resolveWithTimeout:0];
}

- (NSNetService *)service {
    return self.publisher.service;
}

- (void)stop {
    if ([self.delegate respondsToSelector:@selector(bonjourServicesManager:willStopPublishingForServiceId:)]) {
        [self.delegate bonjourServicesManager:self willStopPublishingForServiceId:self.serviceId];
    }
    
    [self.publisher stop];
    [self.subscriber stop];
    
    self.publisher = nil;
    self.subscriber = nil;
    self.delegate = nil;
}

#pragma mark - Dealloc

- (void)dealloc {
    [self stop];
}

@end
