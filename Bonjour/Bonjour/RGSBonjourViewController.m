//
//  RGSViewController.m
//  Bonjour
//
//  Created by Camilo Rodriguez Gaviria on 14/03/14.
//  Copyright (c) 2014 Red Gears Studio. All rights reserved.
//

#import "RGSBonjourViewController.h"
#import "RGSBonjourServicesManager.h"

@interface RGSBonjourViewController () <UITableViewDataSource, UITableViewDelegate, RGSBonjourServicesDelegate>

@property (strong, nonatomic) RGSBonjourServicesManager *bonjourServicesManager;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextView *debug;

- (void)scrollDebugToBottom;

@end

@implementation RGSBonjourViewController

#pragma mark - Life cylce

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.bonjourServicesManager = [[RGSBonjourServicesManager alloc] initWithDelegate:self];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.bonjourServicesManager.services count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.text = [[self.bonjourServicesManager.services objectAtIndex:indexPath.row] hostName];
    cell.textLabel.textColor = [UIColor colorWithWhite:162.f alpha:1];
    
    return cell;
}

#pragma mark - RGSBonjourServicesDelegate

- (void)bonjourServicesManager:(RGSBonjourServicesManager *)bonjourServicesManager didGetServiceId:(NSString *)serviceId {
    self.debug.text = [self.debug.text stringByAppendingString:[NSString stringWithFormat:@"\nService found: %@", serviceId]];
    
    [self.tableView reloadData];
}

- (void)bonjourServicesManager:(RGSBonjourServicesManager *)bonjourServicesManager didGetUpdate:(NSString *)message forServiceId:(NSString *)serviceId {
    self.debug.text = [self.debug.text stringByAppendingString:[NSString stringWithFormat:@"\n[%@] sent: %@ ", serviceId, message]];
    
    [self.tableView reloadData];
}

- (void)bonjourServicesManager:(RGSBonjourServicesManager *)bonjourServicesManager willStopPublishingForServiceId:(NSString *)serviceId {
    self.debug.text = [self.debug.text stringByAppendingString:[NSString stringWithFormat:@"\nService disconnected: %@", serviceId]];
    
    [self.tableView reloadData];
}

- (void)bonjourServicesManager:(RGSBonjourServicesManager *)bonjourServicesManager didServiceStopPublishing:(NSNetService *)service {
    self.debug.text = [self.debug.text stringByAppendingString:[NSString stringWithFormat:@"\nService %@ is no longer available.", [service name]]];
    
    [self.tableView reloadData];
}

#pragma mark - Private

- (void)scrollDebugToBottom {
    CGPoint offset = [self.debug contentOffset];
    [self.debug setContentOffset:offset animated:NO];
    [self.debug scrollRangeToVisible:NSMakeRange([self.debug.text length], 0)];
}

#pragma mark - Dealloc

- (void)dealloc {
    self.tableView.delegate = nil;
    self.tableView.dataSource = nil;
    self.tableView = nil;
    self.debug.delegate = nil;
    self.debug = nil;
    [self.bonjourServicesManager stop];
}

@end
