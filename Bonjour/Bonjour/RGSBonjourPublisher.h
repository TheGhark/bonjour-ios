//
//  RGSBonjourPublisher.h
//  Remark
//
//  Created by Camilo Rodriguez Gaviria on 17/03/14.
//  Copyright (c) 2014 Railwaymen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RGSBonjourServicesManager.h"

@class RGSBonjourPublisher;
@protocol RGSBonjourPublisherDelegate <NSObject>

- (void)bonjourPublisher:(RGSBonjourPublisher *)bonjourPublisher didGetServiceId:(NSString *)serviceId;
- (void)bonjourPublisher:(RGSBonjourPublisher *)bonjourPublisher didGetUpdate:(NSString *)message forServiceId:(NSString *)serviceId;
- (void)bonjourPublisher:(RGSBonjourPublisher *)bonjourPublisher willStopPublishingForServiceId:(NSString *)serviceId;

@end

@interface RGSBonjourPublisher : NSObject <NSNetServiceDelegate>

@property (nonatomic, readonly, strong) NSNetService *service;
@property (nonatomic, weak) id <RGSBonjourPublisherDelegate> delegate;

- (instancetype)initWithBonjourManager:(RGSBonjourServicesManager *)bonjourManager delegate:(id<RGSBonjourPublisherDelegate>)delegate;
- (void)start;
- (void)stop;

@end
