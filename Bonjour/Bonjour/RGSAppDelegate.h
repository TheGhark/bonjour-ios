//
//  RGSAppDelegate.h
//  Bonjour
//
//  Created by Camilo Rodriguez Gaviria on 14/03/14.
//  Copyright (c) 2014 Red Gears Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RGSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
