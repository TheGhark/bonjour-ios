//
//  RGSBonjourSubscriber.h
//  Remark
//
//  Created by Camilo Rodriguez Gaviria on 17/03/14.
//  Copyright (c) 2014 Railwaymen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RGSBonjourServicesManager.h"

@class RGSBonjourSubscriber;
@protocol RGSBonjourSubscriberDelegate <NSObject>

@optional
- (BOOL)bonjourSubscriber:(RGSBonjourSubscriber *)bonjourSubscriber shouldAddService:(NSNetService *)netService;
- (void)bonjourSubscriber:(RGSBonjourSubscriber *)bonjourSubscriber didRemoveService:(NSNetService *)netService;

@end

@interface RGSBonjourSubscriber : NSObject

@property (nonatomic, strong, readonly) NSMutableArray *services;

@property (nonatomic, weak) id<RGSBonjourSubscriberDelegate>delegate;

- (instancetype)initWithBonjourManager:(RGSBonjourServicesManager *)bonjourManager delegate:(id<RGSBonjourSubscriberDelegate>)delegate;
- (void)start;
- (void)stop;

@end
