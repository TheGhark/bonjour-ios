//
//  main.m
//  Bonjour
//
//  Created by Camilo Rodriguez Gaviria on 14/03/14.
//  Copyright (c) 2014 Red Gears Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RGSAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RGSAppDelegate class]));
    }
}
